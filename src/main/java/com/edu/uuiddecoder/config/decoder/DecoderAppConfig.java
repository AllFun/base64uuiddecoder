package com.edu.uuiddecoder.config.decoder;

import com.edu.uuiddecoder.config.AppConfig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Validated
@ConfigurationProperties(prefix = "decoder")
public class DecoderAppConfig implements AppConfig {
    @NotNull
    private String input;
    @NotNull
    private String output;

}
