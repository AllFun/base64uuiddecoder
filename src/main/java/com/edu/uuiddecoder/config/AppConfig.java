package com.edu.uuiddecoder.config;

import org.springframework.stereotype.Component;

@Component
public interface AppConfig {

    String getInput();

    String getOutput();
}
