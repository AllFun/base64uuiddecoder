package com.edu.uuiddecoder.domain.task;

import com.edu.uuiddecoder.config.decoder.DecoderAppConfig;
import com.edu.uuiddecoder.config.encoder.EncoderAppConfig;
import com.edu.uuiddecoder.infrastructure.service.FileService;
import com.edu.uuiddecoder.infrastructure.service.decoder.DecoderService;
import com.edu.uuiddecoder.infrastructure.service.encoder.EncoderService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.function.Predicate;

import static java.lang.String.format;
import static java.util.Arrays.stream;

@Getter
@RequiredArgsConstructor
public enum TaskConfig {
    ENCODE("ENCODE", new Class[]{EncoderService.class, EncoderAppConfig.class, FileService.class}),
    DECODE("DECODE", new Class[]{DecoderService.class, DecoderAppConfig.class, FileService.class});

    private final String value;

    private final Class[] configs;

    public static TaskConfig from(final String taskName) {
        final Predicate<TaskConfig> valueFilter = taskConfig -> taskConfig.getValue().equalsIgnoreCase(taskName);
        return stream(TaskConfig.values())
                .filter(valueFilter)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(format("Unknown task name %s, should be one of: [%s]", taskName, TaskConfig.values())));
    }
}
