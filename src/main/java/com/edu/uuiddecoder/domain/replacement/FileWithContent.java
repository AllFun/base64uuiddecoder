package com.edu.uuiddecoder.domain.replacement;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.File;


@Getter
@ToString
@RequiredArgsConstructor
public class FileWithContent {

    private final File file;

    @Setter
    private String originalContent;

    @Setter
    private String processedContent;
}
