package com.edu.uuiddecoder.domain.replacement;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@Builder
@EqualsAndHashCode
@RequiredArgsConstructor
public class ReplacementGroupIndexes {
    private final ReplacementIndexes value;
    private final ReplacementIndexes group;
}
