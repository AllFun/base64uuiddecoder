package com.edu.uuiddecoder.domain.replacement;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class ReplacementIndexes {
    private final int start;
    private final int end;

    public static ReplacementIndexes from(final int start, final int end) {
        return new ReplacementIndexes(start, end);
    }
}
