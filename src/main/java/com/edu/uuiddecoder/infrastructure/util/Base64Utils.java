package com.edu.uuiddecoder.infrastructure.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.codec.binary.Base64;

import java.nio.ByteBuffer;
import java.util.UUID;

import static java.util.UUID.fromString;

@UtilityClass
public class Base64Utils {

    private static final Base64 base64 = new Base64();

    private static final String ENCODED_POSTFIX = "==";

    public static String uuidToBase64(final UUID uuid) {
        return uuidToBase64(uuid.toString()) + ENCODED_POSTFIX;
    }

    private static String uuidToBase64(final String uuidStr) {
        final UUID uuid = fromString(uuidStr);
        final ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
        byteBuffer.putLong(uuid.getMostSignificantBits());
        byteBuffer.putLong(uuid.getLeastSignificantBits());
        return base64.encodeBase64URLSafeString(byteBuffer.array());
    }

    public static UUID uuidFromBase64(final String encodedUuid) {
        if (encodedUuid.endsWith(ENCODED_POSTFIX)) {
            final String base64EncodedUuid = encodedUuid.substring(0, encodedUuid.length() - 2);
            return uuidFromBase64L(base64EncodedUuid);
        }
        throw new IllegalArgumentException("Invalid encoded UUID: " + encodedUuid);
    }

    private static UUID uuidFromBase64L(final String encodedUuid) {
        final byte[] bytes = base64.decodeBase64(encodedUuid);
        final ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        return new UUID(byteBuffer.getLong(), byteBuffer.getLong());
    }
}
