package com.edu.uuiddecoder.infrastructure.service.decoder;

import com.edu.uuiddecoder.domain.replacement.FileWithContent;
import com.edu.uuiddecoder.domain.replacement.ReplacementGroupIndexes;
import com.edu.uuiddecoder.infrastructure.service.FileService;
import com.edu.uuiddecoder.infrastructure.service.RegexUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;

import static com.edu.uuiddecoder.infrastructure.util.Base64Utils.uuidFromBase64;
import static java.lang.String.format;

@RequiredArgsConstructor
public class DecoderService implements ApplicationRunner {

    private final FileService fileService;

    @Value("${decoder.input}")
    private File input;

    @Value("${decoder.output}")
    private File output;

    @Value("${decoder.wrap-to-uuid}")
    private boolean wrapToUuid;

    @Override
    public void run(ApplicationArguments args) {
        final List<FileWithContent> filesForProcessing = fileService.getFilesForProcessing(input);
        filesForProcessing.stream()
                .map(this::readContent)
                .map(this::decodeUuids)
                .forEach(this::writeToFile);
    }

    private FileWithContent decodeUuids(final FileWithContent fileWithContent) {
        final List<ReplacementGroupIndexes> sortedProcessingList = RegexUtils.getSortedProcessingList(fileWithContent.getOriginalContent());
        fileWithContent.setProcessedContent(decodeUuids(fileWithContent.getOriginalContent(), sortedProcessingList));
        return fileWithContent;
    }

    private String decodeUuids(final String content, final List<ReplacementGroupIndexes> indexes) {
        final StringBuilder stringBuilder = new StringBuilder(content);
        indexes.forEach(replacement -> {
            final String encodedUuid = stringBuilder.substring(replacement.getValue().getStart(), replacement.getValue().getEnd());
            final UUID uuid = uuidFromBase64(encodedUuid);
            stringBuilder.replace(replacement.getGroup().getStart(), replacement.getGroup().getEnd(), getUuidForReplace(uuid));
        });

        return stringBuilder.toString();
    }

    @SneakyThrows
    private FileWithContent readContent(final FileWithContent file) {
        file.setOriginalContent(Files.readString(file.getFile().toPath(), Charset.defaultCharset()));
        return file;
    }

    @SneakyThrows
    private void writeToFile(final FileWithContent content) {
        Files.writeString(new File(output, content.getFile().getName()).toPath(), content.getProcessedContent());
    }

    private String getUuidForReplace(final UUID uuid) {
        final String uuidValue = format("\"%s\"", uuid.toString());
        return wrapToUuid ? format("UUID(%s)", uuidValue) : uuidValue;
    }
}
