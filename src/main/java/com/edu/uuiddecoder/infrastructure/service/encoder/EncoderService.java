package com.edu.uuiddecoder.infrastructure.service.encoder;

import com.edu.uuiddecoder.infrastructure.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.io.File;

@RequiredArgsConstructor
public class EncoderService implements ApplicationRunner {

    private final FileService fileService;

    @Value("${encoder.input}")
    private File input;

    @Value("${encoder.output}")
    private File output;

    @Override
    public void run(ApplicationArguments args) {
        //TODO
    }

}
