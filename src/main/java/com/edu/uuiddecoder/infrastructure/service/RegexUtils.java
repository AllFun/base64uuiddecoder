package com.edu.uuiddecoder.infrastructure.service;

import com.edu.uuiddecoder.domain.replacement.ReplacementGroupIndexes;
import com.edu.uuiddecoder.domain.replacement.ReplacementIndexes;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.edu.uuiddecoder.domain.replacement.ReplacementIndexes.from;
import static java.util.Collections.reverse;

@UtilityClass
public class RegexUtils {

    private static final String VERSIONED_UUID_V4 = "\\{[\\s\\n\\t]{0,}\\\"\\$binary\\\"[\\s\\n\\t]{0,}\\:[\\s\\n\\t]{0,}\\{[\\s\\n\\t]{0,}\\\"base64\\\"[\\s\\n\\t]{0,}\\:[\\s\\n\\t]{0,}\\\"(\\w{22}==)\\\"[\\s\\n\\t]{0,}\\,[\\s\\n\\t]{0,}\\\"subtype\\\"[\\s\\n\\t]{0,}\\:[\\s\\n\\t]{0,}\\\"04\\\"[\\s\\n\\t]{0,}\\}[\\s\\n\\t]{0,}\\}";

    private static final Pattern VERSIONED_UUID_V_4_PATTERN = Pattern.compile(VERSIONED_UUID_V4);

    public static List<ReplacementGroupIndexes> getSortedProcessingList(final String content) {
        final List<ReplacementGroupIndexes> result = new ArrayList<>();
        final Matcher matcher = VERSIONED_UUID_V_4_PATTERN.matcher(content);
        while (matcher.find()) {
            result.add(ReplacementGroupIndexes.builder()
                    .group(getForMatcher(matcher, 0))
                    .value(getForMatcher(matcher, 1))
                    .build());
        }
        reverse(result);

        return result;
    }

    private static ReplacementIndexes getForMatcher(final Matcher matcher, final int group) {
        return from(matcher.start(group), matcher.end(group));
    }
}
