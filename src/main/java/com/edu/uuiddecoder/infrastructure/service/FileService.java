package com.edu.uuiddecoder.infrastructure.service;

import com.edu.uuiddecoder.config.AppConfig;
import com.edu.uuiddecoder.domain.replacement.FileWithContent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class FileService {

    private final AppConfig appConfig;

    public List<FileWithContent> getFilesForProcessing(final File input) {
        //TODO use appConfig instead of parameters
//        return new ArrayList(asList(requireNonNull(new File(appConfig.getInput()).listFiles())));
        return Stream.of(requireNonNull(input.listFiles()))
                .map(FileWithContent::new)
                .collect(Collectors.toList());
    }
}
