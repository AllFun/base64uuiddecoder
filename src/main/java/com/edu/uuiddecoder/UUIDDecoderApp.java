package com.edu.uuiddecoder;

import com.edu.uuiddecoder.domain.task.TaskConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

import static java.lang.String.join;
import static java.util.Optional.ofNullable;
import static org.springframework.boot.WebApplicationType.NONE;

@Slf4j
@SpringBootApplication
@ConfigurationPropertiesScan
public class UUIDDecoderApp {

    private static final String TASK_ARG = "task";

    public static void main(final String[] args) {
        try {
            log.info("Starting...");
            final DefaultApplicationArguments arguments = new DefaultApplicationArguments(args);
            final String taskName = ofNullable(arguments.getOptionValues(TASK_ARG)).map(list -> list.get(0))
                    .orElseThrow(() -> new IllegalArgumentException("Please add -D" + TASK_ARG + "=... \nActual args: " + join("\n", args)));
            final TaskConfig taskConfig = TaskConfig.from(taskName);

            new SpringApplicationBuilder(taskConfig.getConfigs())
                    .web(NONE)
                    .run(args);

        } catch (final Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }
}

