package com.edu.uuiddecoder;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class UUIDDecoderAppTest {

    private static Stream<Arguments> shouldHaveTaskParams() {
        return Stream.of(
                arguments(null, "Args must not be null"),
                arguments(new String[]{""}, "Please add -Dtask=..."),
                arguments(new String[]{"-Dtask=any"}, "Please add -Dtask=..."),
                arguments(new String[]{"task=any"}, "Please add -Dtask=...")
        );
    }

    @ParameterizedTest
    @MethodSource("shouldHaveTaskParams")
    void shouldHaveTask(final String[] args, final String errorMessage) {
        assertThatThrownBy(() -> UUIDDecoderApp.main(args))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining(errorMessage);
    }
}