package com.edu.uuiddecoder.infrastructure.service;

import com.edu.uuiddecoder.domain.replacement.ReplacementGroupIndexes;
import org.junit.jupiter.api.Test;

import static com.edu.uuiddecoder.domain.replacement.ReplacementIndexes.from;
import static com.edu.uuiddecoder.infrastructure.service.RegexUtils.getSortedProcessingList;
import static org.assertj.core.api.Assertions.assertThat;

class RegexUtilsTest {

    private static final String testContent = "[\n" +
            "  {\n" +
            "    \"_id\": {\n" +
            "      \"$binary\": {\n" +
            "        \"base64\": \"M6duDBhvQqeLKz787aLeUg==\",\n" +
            "        \"subtype\": \"04\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"name\": \"Any name1\",\n" +
            "    \"code\": \"AnyCode1\",\n" +
            "    \"status\": \"ACTIVE_TEST_STATUS\"\n" +
            "  }]";

    @Test
    void shouldIdenfityIndexes() {
        assertThat(getSortedProcessingList(testContent))
                .containsExactly(ReplacementGroupIndexes.builder()
                        .group(from(17, 121))
                        .value(from(57, 81))
                        .build());
    }
}