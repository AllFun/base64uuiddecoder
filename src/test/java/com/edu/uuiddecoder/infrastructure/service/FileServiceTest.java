package com.edu.uuiddecoder.infrastructure.service;

import com.edu.uuiddecoder.config.AppConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FileServiceTest {

    @Mock
    private AppConfig appConfig;

    @InjectMocks
    private FileService fileService;

    @Test
    void shouldReturnFiles() {
        final File mock = mock(File.class);
        final File[] files = {new File("a"), new File("b")};
        when(mock.listFiles()).thenReturn(files);

        assertThat(fileService.getFilesForProcessing(mock))
                .extracting("file")
                .containsExactlyInAnyOrder(files);
    }
}