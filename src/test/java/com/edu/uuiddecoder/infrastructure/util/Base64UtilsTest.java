package com.edu.uuiddecoder.infrastructure.util;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static com.edu.uuiddecoder.infrastructure.util.Base64Utils.uuidFromBase64;
import static com.edu.uuiddecoder.infrastructure.util.Base64Utils.uuidToBase64;
import static org.assertj.core.api.Assertions.assertThat;

class Base64UtilsTest {

    private final String uuidStr = "639e3779-7500-4d48-9a28-26f44db2d237";
    private final String uuidEncoded = "Y543eXUATUiaKCb0TbLSNw==";
    private final UUID uuid = UUID.fromString(uuidStr);

    @Test
    void shouldEncodeUuid() {
        assertThat(uuidToBase64(uuid))
                .isEqualTo(uuidEncoded);
    }

    @Test
    void shouldDecodeUuid() {
        assertThat(uuidFromBase64(uuidEncoded))
                .isEqualTo(uuid);
    }
}